import numpy as np

print 'Lab - I, 1.'
def _(a = 1, b = 1, N = 3):    
    xk = np.zeros(N, np.dtype(float))
    yk = np.zeros(len(xk), np.dtype(float))
    for i in range(0, N):
        xk[i] = np.random.uniform(0,1)                
        yk[i] = (b - a) * xk[i] + a
        
    print 'X[',N,'] = ', xk[N-1]
    print 'Y[',N,'] = ', yk[N-1],'\n'
    
    print 'X(M) = ', np.mean(xk)
    print 'X(D) = ', np.std(xk)
    print 'X(sigma^2) = ', np.var(xk),'\n'
    print 'Y(M) = ', np.mean(yk)
    print 'Y(D) = ', np.std(yk)
    print 'Y(sigma^2) = ', np.var(yk)
    
print 'Lab - I, 2.'
def __(m = 3, a = 1, b = 1):
    Z = np.zeros(m-1)
    Xk = np.zeros(len(Z))
    Yk = np.zeros(len(Z), np.dtype(float))
    def __c(Z0 = 0, Z1 = 1):
        Z[0] = Z0
        Z[1] = Z1
        for i in range(2, m-1):
            Z[i] = Z[i-1] + Z[i-2]
            Xk[i] = Z[i] / m
            Yk[i] = (b - a) * Xk[i] + a
        Out = Z[m-2]%m                        
        
        print 'Z[',m-1,'] = ',Out
        print Z
        print 'Z(M) = ', np.mean(Z)
        print 'Z(D) = ', np.std(Z)
        print 'Z(sigma^2) = ', np.var(Z), '\n'
        
        print 'X[',m-1,'] = ', Xk[m-2]
        print 'Y[',m-1,'] = ', Xk[m-2],'\n'
        
        print 'X(M) = ', np.mean(Xk)
        print 'X(D) = ', np.std(Xk)
        print 'X(sigma^2) = ', np.var(Xk),'\n'
        print 'Y(M) = ', np.mean(Yk)
        print 'Y(D) = ', np.std(Yk)
        print 'Y(sigma^2) = ', np.var(Yk)









