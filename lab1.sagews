︠8a00715c-c301-4be7-87ec-e3d1a3c6421fs︠
import numpy as np
import matplotlib.pyplot as plt

print 'Lab - I, 1.'
@interact
def _(a = 1, b = 1, N = 3):    
    xk = np.zeros(N, np.dtype(float))
    yk = np.zeros(len(xk), np.dtype(float))
    for i in range(0, N):
        xk[i] = np.random.uniform(0,1)                
        yk[i] = (b - a) * xk[i] + a
        
    print 'X[',N,'] = ', xk[N-1]
    print 'Y[',N,'] = ', yk[N-1],'\n'
    
    print 'X(M) = ', np.mean(xk)
    print 'X(D) = ', np.std(xk)
    print 'X(σ^2) = ', np.var(xk),'\n'
    print 'Y(M) = ', np.mean(yk)
    print 'Y(D) = ', np.std(yk)
    print 'Y(σ^2) = ', np.var(yk)
    
print 'Lab - I, 2.'
@interact
def __(m = 3, a = 1, b = 1):
    Z = np.zeros(m-1)
    Xk = np.zeros(len(Z))
    Yk = np.zeros(len(Z), np.dtype(float))
    @interact
    def __c(Z0 = 0, Z1 = 1):
        Z[0] = Z0
        Z[1] = Z1
        for i in range(2, m-1):
            Z[i] = Z[i-1] + Z[i-2]
            Xk[i] = Z[i] / m
            Yk[i] = (b - a) * Xk[i] + a
        Out = Z[m-2]%m                        
        
        print 'Z[',m-1,'] = ',Out
        print Z
        print 'Z(M) = ', np.mean(Z)
        print 'Z(D) = ', np.std(Z)
        print 'Z(σ^2) = ', np.var(Z), '\n'
        
        print 'X[',m-1,'] = ', Xk[m-2]
        print 'Y[',m-1,'] = ', Xk[m-2],'\n'
        
        print 'X(M) = ', np.mean(Xk)
        print 'X(D) = ', np.std(Xk)
        print 'X(σ^2) = ', np.var(Xk),'\n'
        print 'Y(M) = ', np.mean(Yk)
        print 'Y(D) = ', np.std(Yk)
        print 'Y(σ^2) = ', np.var(Yk)
︡0e2f557b-6e7b-4f2f-a203-48619177e2de︡{"stdout":"Lab - I, 1.\n"}︡{"interact":{"style":"None","flicker":false,"layout":[[["a",12,null]],[["b",12,null]],[["N",12,null]],[["",12,null]]],"id":"31f0c6d4-ff86-40a9-82f4-94c2e5454cdb","controls":[{"control_type":"input-box","default":1,"label":"a","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"a","type":null},{"control_type":"input-box","default":1,"label":"b","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"b","type":null},{"control_type":"input-box","default":3,"label":"N","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"N","type":null}]}}︡{"stdout":"Lab - I, 2.\n"}︡{"interact":{"style":"None","flicker":false,"layout":[[["m",12,null]],[["a",12,null]],[["b",12,null]],[["",12,null]]],"id":"a5310374-db1c-46d5-9eaf-04dbd078ee97","controls":[{"control_type":"input-box","default":3,"label":"m","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"m","type":null},{"control_type":"input-box","default":1,"label":"a","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"a","type":null},{"control_type":"input-box","default":1,"label":"b","nrows":1,"width":null,"readonly":false,"submit_button":null,"var":"b","type":null}]}}︡
︠4901f0c8-0d36-4ecd-8e7d-03f62d56109e︠









